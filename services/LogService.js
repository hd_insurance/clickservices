const HDIRequest = require("./request");

const log_service = "https://dev-hyperservices.hdinsurance.com.vn/logs"

const insert = async (req_id, log_title, evt, log_type, content)=>{
	return new Promise(async (resolve, rejected)=>{
		try{
			const rq = new HDIRequest();
			let source = null
	 		const e = new Error();
		  const regex = /\((.*):(\d+):(\d+)\)$/
		  const match = regex.exec(e.stack.split("\n")[2]);
		  if(match){
		  	if(match.input){
		  		source = match.input.toString()
		  	}
		  }
			const logs_data = {
			    "group_id": req_id,
			    "app": "61c3df437e11efaaab0d970b",
			    "title": log_title,
			    "source":source,
			    "severity": evt,
			    "type": log_type,
			    "content": content
			}
			const response = await rq.normalSend(log_service, logs_data, "post")
			return resolve(response.log_id)
		}catch(e){
			console.log(e)
			return resolve(null)
		}
	})
	
}

const makeid = () => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 16; i++ ) {
      result += characters.charAt(Math.floor(Math.random() *  charactersLength));
   }
   return result;
}

module.exports = {
	insert,
	makeid
}