const envVariables = {
  domain1: process.env.DM1,
  domain2: process.env.DM2,
  app:{
    ENVIRONMENT: "LIVE"
  },
  redis:{
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT*1,
    password: process.env.REDIS_PASSWORD,
    db: process.env.REDIS_DB.toString(),
    cdb: process.env.REDIS_DB.toString(),
    ttl: 5
  },
  auth:{
      deviceCode: "",
      deviceEnvironment: "WEB",
      ipPrivate: "123",
      parent_code: process.env.PTN_CODE,
      username: process.env.USR_NAME,
      secret: process.env.SECRET,
      password: process.env.PASSWORD,
      CLIENT_ID: process.env.CLIENT_ID,
      token: null,
      lastLogin: 0,
      expireIn: 0
  }
  
}
module.exports = envVariables;