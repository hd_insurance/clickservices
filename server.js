require('dotenv').config()
const passport = require('passport')
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./router');
const cors = require('cors');
const RedisConnector = require('./services/RedisConnector.js');

const PORT = process.env.PORT

const app = express();
var server = require("http").Server(app);

app.use(routes);

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(cors());
app.options("*", cors());

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');


RedisConnector.createConnection((isConnected)=>{
	if(isConnected){
		console.log("Redis Connected")
	}else{
		console.log("Redis Error", isConnected)
	}
	
})


server.listen(PORT, () => {
	console.info(`CLICK SERVICE START AT PORT ${PORT}`)
});