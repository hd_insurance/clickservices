const Router = require('express').Router();
const mainController = require('../controller/Main');
const ShortLinkController = require('../controller/ShortLink');

Router.get('/', function(req, res, next){
	res.redirect('http://hdinsurance.com.vn')
})
Router.get('/credify/logout', mainController.logout)
Router.post('/credify/get-user-existence', mainController.checkUserExistence)
Router.get('/credify/oidc', mainController.oidc)
Router.post('/credify/oidc', mainController.oidcHandle)
Router.all('/credify/callback', mainController.callback)




Router.all('/credify/get-campaigns', mainController.getCampaigns)
Router.all('/credify/get-offer-detail', mainController.getOfferConfig)

Router.all('/credify/test-config', mainController.testGetConfig)


Router.get('/credify/test-token', mainController.createTestToken)
Router.post('/credify/verify-token', mainController.updateStatus)


//shortlink
Router.get('/s/:shortcode', ShortLinkController.getLink)


module.exports = Router;