const {
    Credify
} = require("@credify/nodejs");
const SimpleNodeLogger = require('simple-node-logger')
const base64url = require('base64url');
const Util = require('../services/util.js');
const RedisConnector = require('../services/RedisConnector.js')
const {
    v4: uuidv4
} = require('uuid');
const jwt = require('jsonwebtoken');
const log_opts = {
    logFilePath: 'credify_request.log',
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
}
const log = SimpleNodeLogger.createSimpleLogger(log_opts);
const LogService = require("../services/LogService");
const HDIRequest = require('../services/request');
const config = require('../config');
// You obtain these on the serviceX dashboard.
const signingPrivateKey = `-----BEGIN PRIVATE KEY----- ${process.env.SIGN_PRIVATE_KEY} -----END PRIVATE KEY-----`;
const apiKey = process.env.CRE_API_KEY;
const id = process.env.COMPANY_ID;
const scopes = ["openid"];
const redirectUrl = process.env.REDIRECT_URL_CALLBACK;
const mode = process.env.CREDIFY_MODE

const oidc = async (req, res) => {
    try {
        const credify = await Credify.create(signingPrivateKey, apiKey, {
            mode: mode
        });
        // Generates a random value to identify this request afterwards.
        const state = uuidv4();
        const responseType = "code"; // "token"
        const responseMode = "query"; // "fragment"
        const options = {
            state,
            responseMode,
            responseType
        };
        if (req.query.phone_number) {
            options.phoneNumber = req.query.phone_number;
        } else if (req.query.entity_id) {
            options.userId = req.query.entity_id;
        }
        if (req.query.offer_code) {
            options.offerCode = req.query.offer_code;
        }
        if (req.query.package_code) {
            options.package_code = req.query.package_code;
        }
        const {
            oidcUrl,
            privateKey
        } = await credify.oidc.initiateOIDC(id, redirectUrl, scopes, options);

        options.privateKey = privateKey;
        //Store state redis
        const client = RedisConnector.getClient()
        const redis_key = `credreq_${state}`
        await client.set(`${redis_key}`, JSON.stringify(options), 'EX', 90); //cache 24h
        //Log request
        delete options.privateKey
        log.info('[oidc] ', options, " | " + new Date().toJSON());

        // You will need to store this `private key` along with `state`.
        // await db.Request.create({
        //   state,
        //   privateKey,
        //   offerCode: options.offerCode || "",
        // });


        res.redirect(oidcUrl);
    } catch (e) {
        const log_id = await LogService.insert(uuidv4(), `OIDC Error`, "error", "request", { source: "Main.oidc", e: JSON.stringify(e) })
        console.log(e)
        res.status(403).send({
            error: true, log_id: "#" + log_id
        })
    }
}


const oidcHandle = async (req, res) => {
    try {
        const credify = await Credify.create(signingPrivateKey, apiKey, {
            mode: mode
        });
        const accessToken = req.body.access_token;
        const state = req.body.state;
        log.info('[oidcHandle] ', state, ' access_token ', access_token, new Date().toJSON());

        // const request = await db.Request.findAll({ where: { state } });
        // if (request.length < 1) {
        //   throw new Error("Request not found.");
        // }
        // const encryptionPrivateKey = request[0].privateKey;

        // const data = await credify.oidc.userinfo(accessToken, encryptionPrivateKey);
        // res.send({ ...data });
        // console.log("oidcHandle ", access_token, state)
        res.send({
            success: true
        })


    } catch (e) {
        const log_id = await LogService.insert(uuidv4(), `OIDC Oidc Handle Error`, "error", "request", { e: JSON.stringify(e) })
        console.log(e)
        res.status(403).send({
            log_id: "#" + log_id,
            error: "OIDC: Handle Error"
        })
    }
}


const getCampaigns = async (req, res) => {
    try {
        const credify = await Credify.create(signingPrivateKey, apiKey, {
            mode: mode
        });

        const response = await credify.offer.getCampaigns()
        // console.log("offerCode ", offerCode, response)
        res.send(response)

    } catch (e) {
        const log_id = await LogService.insert(uuidv4(), `getCampaigns Error`, "error", "request", { e: JSON.stringify(e) })
        console.log(e)
        res.status(403).send({
            log_id: "#" + log_id,
            error: "OIDC: getCampaigns Error"
        })
    }
}

const getOfferConfig = async (req, res) => {
    try {
        const credify = await Credify.create(signingPrivateKey, apiKey, {
            mode: mode
        });
        // const response = await credify.offer.getCampaigns()
        const offerCode = "tai-n-n-365-service-p-b6b458b0-514e-48fd-991a-9306593c860e";

        const response = await credify.offer.getOfferDetail(offerCode)
        // console.log("offerCode ", offerCode, response)
        res.send(response)

    } catch (e) {
        const log_id = await LogService.insert(uuidv4(), `getOfferConfig error`, "error", "request", { e: JSON.stringify(e)})
        console.log(e)
        res.status(403).send({
            log_id: "#" + log_id,
            error: "getOfferConfig Error"
        })
    }
}

const checkUserExistence = async (req, res) => {
    try {
        if (req.header('Authorization')) {
            const credify = await Credify.create(signingPrivateKey, apiKey, {
                mode: mode
            });
            const token = req.header('Authorization').split(" ")[1];
            // await LogService.insert(uuidv4(), `Credify CheckUserExistence`, "info", "request", { full: req.header('Authorization'), token: token })
            const validToken = await credify.auth.introspectToken(token, "claim_provider:read_user_existence");
            if (!validToken) {
                return res.status(401).send({ message: "Unauthorized" });
            }
            return res.send({ data: { exists: false } });
        } else {
            res.status(403).send({
                error: "Header token is required!"
            });
        }
    } catch (e) {
        console.log(e)
        await LogService.insert(uuidv4(), `Credify CheckUserExistence Error`, "error", "request", { error: JSON.stringify(e) })
        res.sendStatus(403)
    }
}


const testGetConfig = async (req, res) => {
    try {

        const rq = new HDIRequest()
        // const query_data = {
        //     p_offer_code: "s-c-kho--365-service-p-439390e9-1618-448d-ae04-f94bfea5de40",
        //     p_provider_code: "039f059a-3d58-4592-b359-834f5fe9a442"
        // }
        // const response_transaction = await rq.send(config.domain1 + '/open-api', query_data, "GET_CREDIFY_CONFIG")

        // const product_detail = response_transaction.data[0][0]
        // const services_scope = response_transaction.data[1]

        var token = jwt.sign({
            org: 'CREDIFYVN',
            sku: 'NHA365_01',
            ref_id: 192
        }, process.env.JWT_SECRET);


        const shortlink_data = {
            P_USERNAME: "",
            P_CHANNEL: "",
            P_SHORT_KEY: "SUCKHOE365",
            P_PARAM: `ref=${token}`,
            P_IS_LIMIT: "",
            P_EFF: "",
            P_EXP: ""
        }
        const response_create_shortlink = await rq.send(config.domain1 + '/open-api', shortlink_data, "CREATE_SHORTLINK")

        console.log("response_create_shortlink ", response_create_shortlink.data[0][0].SHORT_CODE)



        // const insert_data = {
        //     p_openid:  uuidv4(),
        //     p_orgcode: "CREDIFYVN",
        //     p_fname: "AA",
        //     p_lname: "AA",
        //     p_phone: "AA",
        //     p_email: "AA",
        //     p_address: "AA",
        //     p_private_key: "AA",
        //     p_transaction_id: "AA",
        //     p_exchange_code: "AA",
        //     p_obj_product: "AA",
        //     p_product_code: "AA"
        // }


        // const response_transaction = await rq.send(config.domain1 + '/open-api', insert_data, "INSERT_CUSINFO")
        // const ref_id = response_transaction.data[0][0].REFID

        // console.log("response_transaction ", ref_id
        //     )




        // var scope_object = {}
        // services_scope.forEach((scope, index) => {
        //     scope_object[scope.FIELD] = scope.SCOPE
        // })




    } catch (e) {
        console.log(e)
    }


    res.send(true)

}

const getValueParams = (userinfo, scope_object) => {

    var address = ""
    var first_name = ""
    var last_name = ""
    var phone = ""
    var city = ""
    if (scope_object.address) {
        if (userinfo[scope_object.address]) {
            address = userinfo[scope_object.address]
        }
    }
    if (scope_object.first_name) {
        if (userinfo[scope_object.first_name]) {
            first_name = userinfo[scope_object.first_name]
        }
    }
    if (scope_object.last_name) {
        if (userinfo[scope_object.last_name]) {
            last_name = userinfo[scope_object.last_name]
        }
    }
    if (scope_object.phone) {
        if (userinfo[scope_object.phone]) {
            phone = userinfo[scope_object.phone]
        }
    }
    if (scope_object.city) {
        if (userinfo[scope_object.city]) {
            city = userinfo[scope_object.city]
        }
    }

    return {
        address: address,
        addressString: Credify_objectAdressToString(address),
        first_name: first_name,
        last_name: last_name,
        phone: phone,
        city: city
    }
}

function Credify_objectAdressToString(obj) {
    // obj = {
    //     "country": "VNM",
    //     "locality": "Quận 1",
    //     "region": "Hồ Chí Minh",
    //     "street_address": "190 Nguyễn Công Trứ"
    //   }
    let _res = `${obj.street_address || ''} ${obj.locality || ''} ${obj.region || ''}`;
    _res = _res.replace(/  +/g, ' ');
    return _res === ' ' ? '' : _res;
}

function formatStringDate(stringDate) { // yy-mm-dd
    try {
        if (stringDate == null || stringDate == undefined) {
            return "";
        }

        const _spDate = stringDate.trim().split('-');
        if (_spDate.length < 3) return "";
        return `${_spDate[2]}/${_spDate[1]}/${_spDate[0]}`;
    } catch (error) {
        console.log(error)
    }
}
function formartName(family_name, middle_name, given_name) {
    res = `${family_name || ''} ${middle_name || ''} ${given_name || ''}`;
    res = res.replace(/\s+/g, ' ');
    return res;
}
const callback = async (req, res) => {
    const req_id = uuidv4()
    let logs_data = {}
    try {
        await LogService.insert(req_id, `OIDC Callback request`, "info", "request", { query: req.query })
        const credify = await Credify.create(signingPrivateKey, apiKey, {
            mode: mode
        });
        const rq = new HDIRequest()
        logs_data.id = id
        logs_data.redirectUrl = redirectUrl
        const response = await credify.oidc.generateAccessToken(id, req.query.code, redirectUrl)
        const access_token = response.access_token
        logs_data.access_token = access_token

        const client = RedisConnector.getClient()
        const redis_key = `credreq_${req.query.state}`
        const ifo = await client.get(`${redis_key}`); //cache 24h
        const package_code = ifo.package_code

        const session_info = JSON.parse(ifo)

        logs_data.session_info = session_info
        const userinfo = await credify.oidc.userinfo(access_token, session_info.privateKey)
        logs_data.userinfo = userinfo

        const offer_info = await credify.offer.getOfferDetail(session_info.offerCode)
        const providerId = offer_info.providerId;

        const query_data = {
            p_offer_code: session_info.offerCode,
            p_provider_code: providerId
        }
        const response_get_config = await rq.send(config.domain1 + '/open-api', query_data, "GET_CREDIFY_CONFIG")
        const product_detail = response_get_config.data[0][0]
        const services_scope = response_get_config.data[1]
        // await LogService.insert(req_id, `OIDC Callback GET_CREDIFY_CONFIG line 357`, "warning", "request", {getOfferDetail: offer_info, query_data:query_data, response_get_config: response_get_config })

        if (!product_detail) {
            const log_id3 = await LogService.insert(req_id, `OIDC Callback Error`, "warning", "request", { e: "Offer/Product not configured yet!", data: query_data, config: response_get_config })
            return res.status(403).send({ error: true, log_id: "#" + log_id3, error: "Offer/Product not configured yet!" })
        }
        var scope_object = {}
        services_scope.forEach((scope, index) => {
            scope_object[scope.FIELD] = scope.SCOPE
        })
        const product_code = product_detail.PRODUCT_CODE
        const product_sku = product_detail.MAR_GROUP
        const product_package = null
        if (package_code) {
            const a = response_get_config.data[0].filter(pack => pack.MAR_PACK_CODE == package_code);
            if (a[0]) {
                if (a[0].PACK_CODE) {
                    product_package = a[0].PACK_CODE
                }
            }
        }



        console.log("-----------------------------------")
        console.log("Offer Code ", session_info.offerCode)
        console.log(userinfo)
        console.log("get config: ", services_scope)
        console.log("product_code ", product_detail)
        console.log("pack_code ", product_package)
        console.log("-----------------------------------")


        const parseValue = getValueParams(userinfo, scope_object)

        var user_object = {};

        // await LogService.insert(req_id, `Credify Scope parse `, "info", "request", { source: "Main.callback", userinfo: userinfo, scope_object: scope_object })

        const buyer_info = {
            "CUS_ID": "",
            "TYPE": "CN",
            "NATIONALITY": "",
            "NAME": formartName(userinfo.family_name, userinfo.middle_name, userinfo.given_name),
            "DOB": formatStringDate(userinfo.birthdate), // 2020-10-05  -> 19/02/1996
            "GENDER": "",
            "PROV": "",
            "DIST": "",
            "WARDS": "",
            "ADDRESS": parseValue.addressString,
            "IDCARD": "",
            "EMAIL": userinfo.email ? userinfo.email : "",
            "PHONE": userinfo.phone_number ? userinfo.phone_number : parseValue.phone,
            "FAX": "",
            "TAXCODE": ""
        }
        await LogService.insert(req_id, `Credify Buyer`, "info", "request", {scope_object: scope_object, parseValue: parseValue, buyer_info: buyer_info})

        if (product_code == "SUCKHOE365" || product_code == "TAINAN365") {
            user_object = {
                "BUYER": buyer_info,
                "HEALTH_INSUR": [],
                "PAY_INFO": {
                    "PAYMENT_TYPE": "CTT"
                }
            }
        } else if (product_code == "NHA365") {
            user_object = {
                "CHANNEL": "SKD",
                "USERNAME": "Ad",
                "ACTION": "BH_M",
                "CATEGORY": "TS10",
                "PRODUCT_CODE": "NHA365",
                "SELLER": {
                    "SELLER_CODE": "Seller_TN",
                    "ORG_CODE": "CREDIFYVN",
                    "ORG_TRAFFIC": "",
                    "TRAFFIC_LINK": "",
                    "ENVIROMENT": "WEB"
                },
                "BUYER": buyer_info,
                "HOUSE_INSUR": []
            }
        }

        const insert_data = {
            p_openid: session_info.userId ? session_info.userId : uuidv4(),
            p_orgcode: "CREDIFYVN",
            p_fname: userinfo.given_name,
            p_lname: userinfo.family_name,
            p_phone: userinfo.phone_number,
            p_email: userinfo.email,
            p_address: parseValue.addressString,
            p_private_key: session_info.privateKey,
            p_transaction_id: userinfo.transaction_id,
            p_exchange_code: providerId,
            p_obj_product: JSON.stringify(user_object),
            p_product_code: product_code
        }

        await LogService.insert(req_id, `OIDC Callback input INSERT_CUSINFO`, "info", "request", {userinfo: userinfo, session_info: session_info, insert_data:insert_data })
        const response_transaction = await rq.send(config.domain1 + '/open-api', insert_data, "INSERT_CUSINFO")
        const p_ref_id = response_transaction.data[0][0].REFID
        var token = jwt.sign({
            org: 'CREDIFYVN',
            sku: product_sku,
            product_package: product_package,
            ref_id: p_ref_id
        }, process.env.JWT_SECRET);

        const shortlink_data = {
            P_USERNAME: "",
            P_CHANNEL: "",
            P_SHORT_KEY: product_code,
            P_PARAM: `ref=${token}`,
            P_IS_LIMIT: "",
            P_EFF: "",
            P_EXP: ""
        }
        // await LogService.insert(req_id, `OIDC Callback CREATE_SHORTLINK input line 483`, "warning", "request", {shortlink_data: shortlink_data })
        const response_create_shortlink = await rq.send(config.domain1 + '/open-api', shortlink_data, "CREATE_SHORTLINK")
        await LogService.insert(req_id, `OIDC Callback CREATE_SHORTLINK`, "info", "request", {response_create_shortlink: response_create_shortlink })

        if (response_create_shortlink.data[0][0].SHORT_CODE) {
            const red_url = `${process.env.SHORT_LINK_URL}/s/${response_create_shortlink.data[0][0].SHORT_CODE}`
            console.log("redirect to ", red_url)

            res.redirect(red_url)
        } else {
            const log_id = await LogService.insert(req_id, `OIDC Callback`, "warning", "request", { source: "Main.callback", message: "CREATE_SHORTLINK: return Null SHORT_CODE" })

            res.send({
                success: false,
                log_id: "#" + log_id
            })
        }
    } catch (e) {
        console.log(e)
        const log_id = await LogService.insert(req_id, `OIDC Callback Error`, "error", "request", { e: JSON.stringify(e), log_data: logs_data })
        res.status(403).send({ error: true, log_id: "#" + log_id, error: e.toString() })
    }
}



const logout = async (req, res) => {
    try {
        log.info('[logout] ', new Date().toJSON());

        res.send({
            success: true
        })
    } catch (e) {
        log.error('[logout] ', e, new Date().toJSON());
        res.sendStatus(403)
    }
}


const updateStatus = async (req, res) => {
    const req_id = uuidv4()
    try {

        if (req.query.token) {
            var decoded = jwt.verify(req.query.token, process.env.JWT_SECRET);
            if (decoded) {
                if (!decoded.order_id) {
                    res.send({
                        success: false,
                        error: "Order info not found"
                    })
                }
                console.log(decoded)
                const rq = new HDIRequest()


                const response_transaction = await rq.send(config.domain1 + '/open-api', {
                    P_REF_ID: 0,
                    P_ORDER_ID: decoded.order_id
                }, "GET_TEMP_CUSINFO")
                if (response_transaction.data[0][0]) {
                    const transaction = response_transaction.data[0][0]
                    const credify = await Credify.create(signingPrivateKey, apiKey, {
                        mode: mode
                    });
                    const status = "COMPLETED";
                    const update_status = await credify.offer.updateStatus(
                        transaction.TRANSACTION_ID,
                        status,
                        {
                            value: decoded.transaction_value,
                            currency: decoded.transaction_currency
                        },
                        decoded.reference_id,
                        {
                            value: decoded.vat_value,
                            currency: decoded.vat_currency
                        }
                    )




                    await LogService.insert(req_id, `Call UPDATE status`, "warning", "request", {
                        TRANSACTION_ID: transaction.TRANSACTION_ID,
                        status: status,
                        amount: {
                            value: decoded.transaction_value,
                            currency: decoded.transaction_currency
                        },
                        reference_id: decoded.reference_id,
                        vat: {
                            value: decoded.vat_value,
                            currency: decoded.vat_currency
                        }
                    })


                    if (update_status) {
                        const response_update_order = await rq.send(config.domain1 + "/open-api", {
                            P_REF_ID: transaction.REF_ID,
                            P_ORDER_ID: transaction.ORDER_CODE,
                            P_STATUS: status
                        },
                            "UPDATE_CONTRACT_CUS_INFO"
                        );
                        res.send({
                            success: true,
                            ref_id: transaction.ref_id,
                            order_id: transaction.ORDER_CODE,
                            transaction_status: status
                        })
                    } else {
                        res.send({
                            success: false,
                            error: "Credify error update status"
                        })
                    }
                } else {
                    res.send({
                        success: false,
                        error: "Transaction Not Found"
                    })
                }
            } else {
                res.send({
                    success: false,
                    error: "Token is invalid"
                })
            }
        } else {
            res.send({
                success: false
            })
        }



    } catch (e) {
        await LogService.insert(req_id, `updateStatus error line 620`, "error", "request", { eror: e.toString() })
        log.error('[Callback] ', e, new Date().toJSON());
        console.log("Update Status ", e)
        res.sendStatus(403)
    }
}


const createTestToken = async (req, res) => {
    try {
        var token = jwt.sign({
            "order_id": "D9D4E1308E541A48E0530100007F5E88",
            "transaction_value": 1400000,
            "transaction_currency": "VND",
            "reference_id": "S22F11CJ6I6V;S22F11CJ9BD3",
            "vat_value": 140000,
            "vat_currency": "VND"
        }, process.env.JWT_SECRET);

        res.send({
            success: token
        })
    } catch (e) {
        log.error('[logout] ', e, new Date().toJSON());
        await LogService.insert(req_id, `createTestToken error line 644`, "error", "request", { eror: e.toString() })
        res.sendStatus(403)
    }
}


module.exports = {
    oidc,
    oidcHandle,
    callback,
    checkUserExistence,
    logout,
    getCampaigns,
    getOfferConfig,
    updateStatus,
    createTestToken,
    testGetConfig
}
