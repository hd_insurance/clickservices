const config = require('../config');
const RedisConnector = require('../services/RedisConnector.js')
const HDIRequest = require('../services/request');
const LogService = require("../services/LogService");
const {v4: uuidv4} = require('uuid');
const getLink = async (req, res)=>{
	try{
		const req_id = uuidv4()
		// const client = RedisConnector.getClient()
		// const redis_key = `SL_${req.params.shortcode}`
		// const redis_value = await client.get(`${redis_key}`);
		// if(redis_value){
		// 	const obj_link = JSON.parse(redis_value)
		// 	return res.redirect(obj_link.ORIGINAL_URL)
		// }else{
			const rq = new HDIRequest()
			const data = {p_code: req.params.shortcode}
			const response = await rq.send(config.domain1+'/open-api', data, "GET_SHORTLINK")
			if(response.success){
				if(response.data[0][0]){
					const shortlink_info = response.data[0][0]
					console.log("shortlink_info ", shortlink_info)
					await LogService.insert(req_id, `Short link: Redirect...`, "info", "request", {ORIGINAL_URL: shortlink_info.ORIGINAL_URL})
					// await client.set(`${redis_key}`, JSON.stringify(shortlink_info), 'EX', 86400); //cache 24h
					return res.redirect(shortlink_info.ORIGINAL_URL)
				}else{
					const log_id = await LogService.insert(req_id, `Short link Error: Not found`, "warning", "request", {short_code: req.params.shortcode})
					return res.render('index', { log_id:log_id });
				}
			// }else{
			// 	res.status(404).send("Link not found")
			// }
		}
		
		
	}catch(e){
		const log_id = await LogService.insert(req_id, `Short link Error`, "warning", "request", {e: e.toString()})
		console.log(e)
		return res.render('index', { log_id:log_id });
	}
}



module.exports = {
	getLink
}